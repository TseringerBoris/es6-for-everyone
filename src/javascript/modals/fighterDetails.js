import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  nameElement.innerText = `Name: ${name}\nHealth: ${health}\nAttack: ${attack}\nDefense: ${defense}`;

  let img = document.createElement("img");
  img.src = source;

  fighterDetails.append(nameElement);
  fighterDetails.append(img);

  return fighterDetails;
}
