import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
  // show winner name and image
    console.log(`winner: ${fighter.name}`);
    const title = 'New Winner';
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });

    nameElement.innerText = fighter.name;

    let img = document.createElement("img");
    img.src = fighter.source;

    bodyElement.append(nameElement);
    bodyElement.append(img);

    showModal({ title, bodyElement });
}