export function fight(firstFighter, secondFighter) {
    // return winner
    let first = Object.assign({}, firstFighter);
    let second = Object.assign({}, secondFighter);

    if (first.health <= 0) {
        return second;
    } else if (second.health <= 0) {
        return first;
    }

    first.health -= getDamage(second, first);
    second.health -= getDamage(first, second);

    return fight(first, second);
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    if(damage < 0){
      damage = 0;
    }
  // return damage
    return damage;
}

export function getHitPower(fighter) {
  // return hit power
    const criticalHitChance = Math.random() + 1.01;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block
    const dodgeChance = Math.random() + 1.01;
    return fighter.defense * dodgeChance;
}
